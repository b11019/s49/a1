//! console.log('Hello World'); 
// ! [FETCH]
// ! Fetch is a function that is used to fetch data from a server
/* fetch(url, options)
    url - the url of the data to be fetched
    options - an object containing options for the fetch request
    returns a promise that resolves to the response object
    response object has the following properties:
        ok - true if the fetch was successful, false if not
        status - the HTTP status code of the response
        statusText - the status text of the response
        url - the url of the response
        type - the type of the response
        body - the body of the response
        json() - returns a promise that resolves to the json body of the response
        text() - returns a promise that resolves to the text body of the response
        blob() - returns a promise that resolves to the blob body of the response
        arrayBuffer() - returns a promise that resolves to the arrayBuffer body of the response
        headers - an object containing the response headers
        headers.get(name) - returns the value of the header with the given name
        clone() - returns a clone of the response object
        error() - returns a promise that resolves to the error object of the response
        redirected - true if the response was redirected, false if not
        redirectedTo - the url redirected to if redirected is true
        redirectedFrom - the url redirected from if redirected is true
        redirectedStatus - the status code of the response if redirected is true
        redirectedStatusText - the status text of the response if redirected is true
        redirectedType - the type of the response if redirected is true
        redirectedBody - the body of the response if redirected is true
        redirectedJson() - returns a promise that resolves to the json body of the response if redirected is true
        redirectedText() - returns a promise that resolves to the text body of the response if redirected is true
        redirectedBlob() - returns a promise that resolves to the blob body of the response if redirected is true
        redirectedArrayBuffer() - returns a promise that resolves to the arrayBuffer body of the response if redirected is true
        redirectedHeaders - an object containing the response headers if redirected is true 
        */

fetch('https://jsonplaceholder.typicode.com/posts').then(response => {
    return response.json();
}).then(data => {
    showPosts(data);
});

showPosts = (posts) => {
    let postEntries = "";
    posts.forEach(post => {
        console.log(post);
        postEntries += `
        <div>
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick='editPost(${post.id})' id = "editPst">Edit</button>
            <button onclick='deletePost(${post.id})' id = "deletePst">Delete</button>
        </div>`;
     })
     document.querySelector('#div-post-entries').innerHTML = postEntries;
    }

// Add post

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
    }),
    headers: {
        'Content-Type': 'application/json'
    }
    }).then(response => { 
        return response.json();
    }).then(data => {
        console.log(data);
        alert('Post added successfully');

        // querySelector = null, resets the state of our input into blanks after submitting a new post
        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null;
    })
})

// edit post

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;


    document.querySelector(`#txt-edit-id`).value = id;
    document.querySelector(`#txt-edit-title`).value = title;
    document.querySelector(`#txt-edit-body`).value = body

    document.querySelector("#btn-submit-update").removeAttribute('disabled');
}

// update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();
    fetch(`https://jsonplaceholder.typicode.com/posts/${document.querySelector('#txt-edit-id').value}`, {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
    }),
    headers: {
        'Content-Type': 'application/json'
    }
    }).then(response => {
        return response.json();
    }).then(data => {
        console.log(data);
        alert('Post updated successfully');

        document.querySelector('#txt-edit-id').value = null;
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;
        document.querySelector("#btn-submit-update").setAttribute('disabled', true);
    }
    )})

    // delete post

    const deletePost = (id) => {
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: 'DELETE'
        }).then(response => {
             document.querySelector(`#post-title-${id}`).remove();
             document.querySelector(`#post-body-${id}`).remove();
             document.querySelector(`#editPst`).remove();
             document.querySelector(`#deletePst`).remove();
            
           
           
            response.text();
        }
        ).then(data => {
            console.log(data);
            alert('Post deleted successfully');
        },
        
        )}
        
